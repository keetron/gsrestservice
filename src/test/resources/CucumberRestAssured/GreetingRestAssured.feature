Feature: Greeting Controller

  Scenario: Call service without parameters using restassured
    Given the greeting service is running
    When the client requests GET /greeting
    Then the status code is 200
    And the response body should contain:
    """
    {"id":1,"content":"Hello, World!"}
    """
