package cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/cucumber/cucumber.json", "html:target/cucumber", "junit:target/cucumber/junit.xml"},
        glue = "cucumber.stepdefs",
        features = "src/test/resources/Cucumber",
        monochrome = true
)
public class RunCucumberIT {

}