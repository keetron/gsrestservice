package cucumber.restassured;

import cucumber.api.PendingException;
import cucumber.api.java8.En;
import hello.Application;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.springframework.boot.SpringApplication;

import static io.restassured.RestAssured.when;

public class GreetingRestAssuredStepDef implements En {

    private Response response;
    private ValidatableResponse json;
    private final String BASE_URL = "http://localhost:8081/greeting";


    public GreetingRestAssuredStepDef() {
        Given("^the greeting service is running$", () -> {
            try {
                SpringApplication.run(Application.class);
            } catch (Throwable t) {
                System.out.println("The service cannot start, it is likely already running.");
            }
        });

        When("^the client requests GET /greeting$", () -> {
            response = when().get(BASE_URL);
            System.out.println("response: " + response.prettyPrint());
        });

        Then("^the status code is (\\d+)$", (Integer statusCode) -> {
            json = response.then().statusCode(statusCode);
        });

        Then("^the response body should contain:$", (String jsonBody) -> {

        });
    }
}
